//Terraform Ressources
resource "aws_key_pair" "keypqirtp" {
  key_name   = "sshkey"
  public_key = file(var.aws_public_key_ssh_path) //To avoid to put the key in hard in the project
}

resource "aws_default_vpc" "vpcTp" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "securityGroupTp" {
  vpc_id = aws_default_vpc.vpcTp.id

  ingress {
    protocol  = "6"
    self      = true
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "6"
    self      = true
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = "sshkey"
  tags = {
    Name = "HelloWorld"
  }
}

//To go Further
//Data Base
resource "aws_dynamodb_table" "tableauTp" {
  name             = "tableauTp"
  hash_key         = "TestTableHashKey"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "TestTableHashKey"
    type = "S"
  }

}

//Policies
resource "aws_iam_role" "role" {
  name = "test-role"

  assume_role_policy = <<EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}
