def division(num, denum) -> int:
    return round(num / denum)
