from flask import Flask
import boto3

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"


def insert_user_into_dynamo(user):
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("tableauTp")
    table.put_item(Item={"id": user["id"], "name": user["name"], "email": user["email"]})


if __name__ == "__main__":
    app.run(host="0.0.0.0")
